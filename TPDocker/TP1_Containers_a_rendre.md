
# Sommaire

- [TP1 : Containers](#tp1--containers)
- [Sommaire](#sommaire)
- [I. Docker](#i-docker)
  - [1. Install](#1-install)
- [II. Images](#ii-images)
  - [2. Construisez votre propre Dockerfile](#2-construisez-votre-propre-dockerfile)
- [III. `docker-compose`](#iii-docker-compose)
  - [1. Intro](#1-intro)
  - [2. WikiJS](#2-wikijs)
  - [3. Make your own meow](#3-make-your-own-meow)
- [IV. Docker security](#iv-docker-security)
  - [1. Le groupe docker](#1-le-groupe-docker)
  - [2. Scan de vuln](#2-scan-de-vuln)
  - [3. Petit benchmark secu](#3-petit-benchmark-secu)


# I. Docker

## 1. Install

🌞 **Installer Docker sur la machine**
```bash
#Setup du Docker repository
sudo yum install -y yum-utils 
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
#Installation du Docker Engine 
sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
 
- démarrer le service `docker` avec une commande `systemctl`
```bash
sudo systemctl start docker
```


🌞 **Utiliser la commande `docker run`**

```bash
docker run --name nginxweb -d --cpus 0.5 --memory 500m -v /home/mirak/laconfdnginx/laconf.conf:/etc/nginx/conf.d/laconf.conf -v /var/www/tp_docker/index.html:/usr/share/nginx/html/index.html -p 8888:80 nginx
```
# II. Images

## 2. Construisez votre propre Dockerfile

🌞 **Construire votre propre image**
- Le fichier Dockerfile
```bash
FROM debian:stable-slim
RUN apt-get update -y
RUN apt-get install -y apache2
RUN mkdir -p /etc/apache2/logs/
RUN chown -R www-data:www-data /etc/apache2/logs/
RUN chmod -R 755 /etc/apache2/logs/
CMD ["/usr/sbin/apache2","-g","daemon off;"]
COPY apache2.conf /etc/apache2/apache2.conf
VOLUME /etc/apache2
COPY index.html /var/www/html/index.html
VOLUME /var/www/html
CMD ["apache2ctl", "-D", "FOREGROUND"]
EXPOSE 80
```

```bash
[mirak@vm MonImageDocker]$ docker build . -t monapache3
[+] Building 1.4s (13/13) FINISHED                                                                                     docker:default
 => [internal] load build definition from Dockerfile                                                                             0.1s
 => => transferring dockerfile: 514B                                                                                             0.0s
 => [internal] load .dockerignore                                                                                                0.0s
 => => transferring context: 2B                                                                                                  0.0s
 => [internal] load metadata for docker.io/library/debian:stable-slim                                                            1.1s
 => [1/8] FROM docker.io/library/debian:stable-slim@sha256:d4494b6af5eafd77c251e7eb1b5bfefc99a7b81172e80c30feb883898f7e5655      0.0s
 => [internal] load build context                                                                                                0.0s
 => => transferring context: 181B                                                                                                0.0s
 => CACHED [2/8] RUN apt-get update -y                                                                                           0.0s
 => CACHED [3/8] RUN apt-get install -y apache2                                                                                  0.0s
 => CACHED [4/8] RUN mkdir -p /etc/apache2/logs/                                                                                 0.0s
 => CACHED [5/8] RUN chown -R www-data:www-data /etc/apache2/logs/                                                               0.0s
 => CACHED [6/8] RUN chmod -R 755 /etc/apache2/logs/                                                                             0.0s
 => CACHED [7/8] COPY apache2.conf /etc/apache2/apache2.conf                                                                     0.0s
 => CACHED [8/8] COPY index.html /var/www/html/index.html                                                                        0.0s
 => exporting to image                                                                                                           0.0s
 => => exporting layers                                                                                                          0.0s
 => => writing image sha256:0150ce395e7f4f96c9a374bffa3d7d5c96b746619de37f272d7c08c3720b6943                                     0.0s
 => => naming to docker.io/library/monapache3
```
## 2. WikiJS


🌞 **Installez un WikiJS** en utilisant Docker
```bash
docker pull linuxserver/wikijs
```
🌞 **Construisez vous-mêmes l'image de WikiJS**
```yaml
---
version: "2.1"
services:
  wikijs:
    image: lscr.io/linuxserver/wikijs:latest
    container_name: wikijs
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Etc/UTC
      - DB_TYPE=sqlite #optional
      - DB_HOST= #optional
      - DB_PORT= #optional
      - DB_NAME= #optional
      - DB_USER= #optional
      - DB_PASS= #optional
    volumes:
      - /path/to/config:/config
      - /path/to/data:/data
    ports:
      - 3000:3000
    restart: unless-stopped
```




## 3. Make your own meow


🌞 **Conteneurisez votre application**

```bash
FROM python:3
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
EXPOSE 8888
CMD ["python", "app.py"]
```


```bash
version: '3'
services:
  app:
    build: .
    ports:
      - "8888:8888"
    container_name: "python"
    depends_on:
      - db
  db:
    image: "redis:latest"
    ports:
      - "6379:6379"
    container_name: "db"
```

```bash
[mirak@vm python]$ ls
pyhton.py  docker-compose.yml  Dockerfile  requirements.txt  templates
[mirak@vm python]$ pwd
/home/mirak/dockerapp/python
```
# IV. Docker security

Dans cette partie, on va survoler quelques aspects de Docker en terme de sécurité.

## 1. Le groupe docker


🌞 **Prouvez que vous pouvez devenir `root`**

```bash
`docker run -v /etc/shadow:/etc/shadow2 debian cat /etc/shadow2`
```

## 2. Scan de vuln

Il existe des outils dédiés au scan de vulnérabilités dans des images Docker.

C'est le cas de [Trivy](https://github.com/aquasecurity/trivy) par exemple.

🌞 **Utilisez Trivy**
# Scan app python
```bash
trivy image app-app

app-app (debian 12.2)
Total: 780 (UNKNOWN: 2, LOW: 500, MEDIUM: 219, HIGH: 56, CRITICAL: 3)

Python (python-pkg)
Total: 1 (UNKNOWN: 0, LOW: 0, MEDIUM: 1, HIGH: 0, CRITICAL: 0)
```
# Scan bdd app
```bash
trivy image redis:latest

redis:latest (debian 12.2)
Total: 184 (UNKNOWN: 1, LOW: 136, MEDIUM: 31, HIGH: 15, CRITICAL: 1)

usr/local/bin/gosu (gobinary)
Total: 4 (UNKNOWN: 0, LOW: 1, MEDIUM: 2, HIGH: 1, CRITICAL: 0)
```
# Scan Apache
```bash
trivy image customimage

customimage (debian 10.13)
Total: 241 (UNKNOWN: 1, LOW: 161, MEDIUM: 33, HIGH: 44, CRITICAL: 2)

/etc/ssl/private/ssl-cert-snakeoil.key (secrets)
Total: 1 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 1, CRITICAL: 0)
```

# Scan nginx
```bash
trivy image nginx

nginx (debian 12.2)
Total: 110 (UNKNOWN: 2, LOW: 81, MEDIUM: 19, HIGH: 7, CRITICAL: 1)
```

## 3. Petit benchmark secu
🌞 **Utilisez l'outil Docker Bench for Security**

  sur ma machine de test 
  ```bash
  Section C - Score
  [INFO] Checks: 88
  [INFO] Score: -1