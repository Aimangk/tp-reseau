# TP2 : Routage, DHCP et DNS

# Sommaire

- [TP2 : Routage, DHCP et DNS](#tp2--routage-dhcp-et-dns)
- [Sommaire](#sommaire)
- [I. Routage](#i-routage)
- [II. Serveur DHCP](#ii-serveur-dhcp)
- [III. ARP](#iii-arp)
  - [1. Les tables ARP](#1-les-tables-arp)

# I. Routage


☀️ **Configuration de `router.tp2.efrei`**
- L'intérface enp0s8 est déstinée au ssh
- Configuration intérface enp0s9 :
```bash
[mirak@router network-scripts]$ cat ifcfg-enp0s9
NAME=enp0s9
DEVICE=enp0s9

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.254
NETMASK=255.255.255.0

GATEWAY=10.2.1.254
DNS1=1.1.1.1
```

- Configuration intérface enp0s3 :
```bash
  [mirak@router network-scripts]$ cat ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3
    
BOOTPROTO=dhcp
ONBOOT=yes
```

- Ip a:
```bash
[mirak@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1b:70:80 brd ff:ff:ff:ff:ff:ff
    inet 192.168.11.130/24 brd 192.168.11.255 scope global dynamic noprefixroute enp0s3
       valid_lft 1479sec preferred_lft 1479sec
    inet6 fe80::a00:27ff:fe1b:7080/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f5:80:35 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.102/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 578sec preferred_lft 578sec
    inet6 fe80::6efe:f421:8c4a:f543/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ea:9e:5e brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feea:9e5e/64 scope link
       valid_lft forever preferred_lft forever
  ```     

On a demander à cette machine Rocky de ne pas jeter les paquets IPs qui ne lui sont pas destinés, **afin qu'elle puisse agir comme un routeur**.
:

```bash
# On active le forwarding IPv4
[mirak@router ~]$ sudo sysctl -w net.ipv4.ip_forward=1 
net.ipv4.ip_forward = 1

# Petite modif du firewall qui nous bloquerait sinon
[mirak@router ~]$ sudo firewall-cmd --add-masquerade
success

# Et on tape aussi la même commande une deuxième fois, en ajoutant --permanent pour que ce soit persistent après un éventuel reboot
[mirak@router ~]$ sudo firewall-cmd --add-masquerade --permanent
success
```

☀️ **Configuration de `node1.tp2.efrei`**

- Configuration de l'intérface enp0s3:
```bash
[mirak@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.1
NETMASK=255.255.255.0

GATEWAY=10.2.1.254
DNS1=1.1.1.1
```  
  
- Ip a:
```bash
[mirak@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:52:aa:ed brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.1/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft 426sec preferred_lft 426sec
    inet6 fe80::a00:27ff:fe52:aaed/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7f:d5:0a brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.103/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 426sec preferred_lft 426sec
    inet6 fe80::737c:8d89:b107:143d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
- ping vers routeur :
```bash
[mirak@node1 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=3.10 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=4.29 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=24.7 ms

--- 10.2.1.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2006ms
rtt min/avg/max/mdev = 3.097/10.686/24.667/9.898 ms
```
- ping vers internet :
```bash
[mirak@node1 ~]$ ping google.com
PING google.com (216.58.214.174) 56(84) bytes of data.
64 bytes from mad01s26-in-f174.1e100.net (216.58.214.174): icmp_seq=1 ttl=127 time=26.7 ms
64 bytes from mad01s26-in-f14.1e100.net (216.58.214.174): icmp_seq=2 ttl=127 time=28.8 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 26.686/27.736/28.786/1.050 ms
```
- trace path montrant que ça passe bien par le routeur :
```bash
[mirak@node1 ~]$ tracepath -b -4 google.com
 1?: [LOCALHOST]                      pmtu 1500
 1:  _gateway (10.2.1.254)                                 3.509ms
 1:  _gateway (10.2.1.254)                                 2.192ms
 2:  192.168.11.2 (192.168.11.2)                           3.654ms
 ```

# II. Serveur DHCP



☀️ **Install et conf du serveur DHCP**
- Configuration de l'intérface enp0s3 :
```bash
[mirak@dhcp ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.2.1.253
NETMASK=255.255.255.0

GATEWAY=10.2.1.254
DNS1=1.1.1.1
```

- Installation du serveur dhcp :
```bash
dnf -y install dhcp-server
```
- Configuration du serveur dhcp
```bash
[mirak@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for mirak:
# create new
# specify domain name
option domain-name     "srv.world";
# specify DNS server's hostname or IP address
option domain-name-servers     1.1.1.1;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.200 10.2.1.252;
    # specify broadcast address
    option broadcast-address 10.2.1.255;
    # specify gateway
    option routers 10.2.1.254;
}
```

☀️ **Test du DHCP** sur `node1.tp2.efrei`
- Configuration de l'intérface enp0s3 :
```bash
[mirak@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=dhcp
ONBOOT=yes
```
- Ip a :
```bash
[mirak@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:52:aa:ed brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.200/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 398sec preferred_lft 398sec
    inet6 fe80::a00:27ff:fe52:aaed/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:7f:d5:0a brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.103/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 444sec preferred_lft 444sec
    inet6 fe80::737c:8d89:b107:143d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
- Ping internet :
```bash
[mirak@node1 ~]$ ping google.com
PING google.com (142.250.179.78) 56(84) bytes of data.
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=1 ttl=127 time=30.2 ms
64 bytes from par21s19-in-f14.1e100.net (142.250.179.78): icmp_seq=2 ttl=127 time=55.7 ms
^C
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 30.160/42.934/55.708/12.774 ms
```
- table de routage :
```bash
[mirak@node1 ~]$ ip route show
default via 10.2.1.254 dev enp0s3 proto dhcp src 10.2.1.200 metric 100
10.2.1.0/24 dev enp0s3 proto kernel scope link src 10.2.1.200 metric 100
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.103 metric 101
```




☀️ **Wireshark it !**

- Voir capture [DORA](dora.pcapng)
  

# III. ARP

## 1. Les tables ARP



☀️ **Affichez la table ARP de `router.tp2.efrei`**

```bash
[mirak@router ~]$ ip neigh show
192.168.56.101 dev enp0s8 lladdr 0a:00:27:00:00:06 REACHABLE
51.145.123.29 dev enp0s9 FAILED
188.165.49.6 dev enp0s9 FAILED
80.74.64.2 dev enp0s9 FAILED
194.177.34.116 dev enp0s9 FAILED
10.2.1.200 dev enp0s9 lladdr 08:00:27:52:aa:ed REACHABLE
5.196.160.139 dev enp0s9 FAILED
192.168.11.2 dev enp0s3 lladdr 00:50:56:f4:3b:6b STALE
192.168.11.254 dev enp0s3 lladdr 00:50:56:f3:21:03 REACHABLE
10.2.1.1 dev enp0s9 FAILED
10.2.1.253 dev enp0s9 lladdr 08:00:27:e0:e6:17 STALE
192.168.56.100 dev enp0s8 lladdr 08:00:27:ba:6e:27 REACHABLE
```

☀️ **Capturez l'échange ARP avec Wireshark**

  - Capture [ARP](arp.pcapng)

