# Mise en place de la topologie et routage


## I. Setup GNS3

🌞 **Mettre en place la topologie dans GS3**


  - toutes les machines du réseau 1 peuvent se `ping` entre elles :<br />
  node1.net.tp3:
  
  ```bash
 [mirak@node1 ~]$ hostname
node1.net1.tp3
[mirak@node1 ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=7.87 ms
^C
--- 10.3.1.12 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 7.865/7.865/7.865/0.000 ms
[mirak@node1 ~]$ ping 10.3.1.254
PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=3.80 ms
^C
--- 10.3.1.254 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 3.800/3.800/3.800/0.000 ms
  ```
  node2.net1.tp3:
  ```bash
  [mirak@node2 ~]$ hostname
node2.net1.tp3
[mirak@node2 ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=3.60 ms
^C
--- 10.3.1.11 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 3.596/3.596/3.596/0.000 ms
[mirak@node2 ~]$ ping 10.3.1.254
PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=5.48 ms
^C
--- 10.3.1.254 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 5.477/5.477/5.477/0.000 ms
  ```
  router.net1.tp3 :
```bash
[mirak@router ~]$ hostname
router.net1.tp3
[mirak@router ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=4.07 ms
^C
--- 10.3.1.11 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 4.068/4.068/4.068/0.000 ms
[mirak@router ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=2.70 ms
^C
--- 10.3.1.12 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 2.702/2.702/2.702/0.000 ms
  ```
  

  - toutes les machines du réseau 2 peuvent se `ping` entre elles :<br />

node1.net2.tp3 :
```bash
[mirak@node1 ~]$ hostname
node1.net2.tp3
[mirak@node1 ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=7.79 ms
^C
--- 10.3.2.12 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 7.794/7.794/7.794/0.000 ms
[mirak@node1 ~]$ ping 10.3.2.254
PING 10.3.2.254 (10.3.2.254) 56(84) bytes of data.
64 bytes from 10.3.2.254: icmp_seq=1 ttl=64 time=10.6 ms
^C
--- 10.3.2.254 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 10.642/10.642/10.642/0.000 ms
```
node2.net2.tp3 :
```bash 
[mirak@node2 ~]$ hostname
node2.net2.tp3
[mirak@node2 ~]$ ping 10.3.100.2
PING 10.3.100.2 (10.3.100.2) 56(84) bytes of data.
64 bytes from 10.3.100.2: icmp_seq=1 ttl=64 time=3.37 ms
^C
--- 10.3.100.2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 3.366/3.366/3.366/0.000 ms
[mirak@node2 ~]$ ping 10.3.2.11
PING 10.3.2.11 (10.3.2.11) 56(84) bytes of data.
64 bytes from 10.3.2.11: icmp_seq=1 ttl=64 time=4.83 ms
^C
--- 10.3.2.11 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 4.833/4.833/4.833/0.000 ms
```
router.net2.tp3 :
```bash
[mirak@router ~]$ hostname
router.net2.tp3
[mirak@router ~]$ ping 10.3.2.11
PING 10.3.2.11 (10.3.2.11) 56(84) bytes of data.
64 bytes from 10.3.2.11: icmp_seq=1 ttl=64 time=3.64 ms
^C
--- 10.3.2.11 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 3.639/3.639/3.639/0.000 ms
[mirak@router ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=1.96 ms
^C
--- 10.3.2.12 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.963/1.963/1.963/0.000 ms
```
  
  - toutes les machines du réseau 3 peuvent se `ping` entre elles :<br />
  
  router.net2.tp3 :
```bash
[mirak@router ~]$ hostname
router.net2.tp3
[mirak@router ~]$ ping 10.3.100.1
PING 10.3.100.1 (10.3.100.1) 56(84) bytes of data.
64 bytes from 10.3.100.1: icmp_seq=1 ttl=64 time=1.91 ms
^C
--- 10.3.100.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.910/1.910/1.910/0.000 ms
```
router.net1.tp3 :
```bash
[mirak@router ~]$ hostname
router.net1.tp3
[mirak@router ~]$ ping 10.3.100.2
PING 10.3.100.2 (10.3.100.2) 56(84) bytes of data.
64 bytes from 10.3.100.2: icmp_seq=1 ttl=64 time=3.40 ms
^C
--- 10.3.100.2 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 3.403/3.403/3.403/0.000 ms
```
- le `router1.tp3` doit avoir un accès internet normal
  
  - prouvez avec une commande `ping` qu'il peut joindre une IP publique connue : </br>
```bash
[mirak@router ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=128 time=614 ms
^C
--- 1.1.1.1 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 613.852/613.852/613.852/0.000 ms
```
  
  - prouvez avec une commande `ping` qu'il peut joindre des machines avec leur nom DNS public (genre `efrei.fr`) : </br>
```bash
[mirak@router ~]$ ping efrei.fr
PING efrei.fr.localdomain (51.255.68.208) 56(84) bytes of data.
64 bytes from ns3028977.ip-51-255-68.eu (51.255.68.208): icmp_seq=2 ttl=128 time=902 ms
^C
--- efrei.fr.localdomain ping statistics ---
4 packets transmitted, 1 received, 75% packet loss, time 4096ms
rtt min/avg/max/mdev = 901.691/901.691/901.691/0.000 ms

```



## II. Routes routes routes

🌞 **Activer le routage sur les deux machines `router`**

```bash
# On active le forwarding IPv4
[it4@router ~]$ sudo sysctl -w net.ipv4.ip_forward=1 
net.ipv4.ip_forward = 1

# Petite modif du firewall qui nous bloquerait sinon
[it4@router ~]$ sudo firewall-cmd --add-masquerade
success

# Et on tape aussi la même commande une deuxième fois, en ajoutant --permanent pour que ce soit persistent après un éventuel reboot
[it4@router ~]$ sudo firewall-cmd --add-masquerade --permanent
success
```

🌞 **Mettre en place les routes locales**

- ajoutez les routes nécessaires pour que les membres du réseau 1 puissent joindre les membres du réseau 2 (et inversement) : 
```bash
[mirak@router ~]$ hostname
router.net1.tp3
[mirak@router ~]$ cat /etc/sysconfig/network-scripts/route-enp0s9
10.3.2.0/24 via 10.3.100.2 dev enp0s9
```
```bash
[mirak@router ~]$ hostname
router.net2.tp3
[mirak@router ~]$ cat /etc/sysconfig/network-scripts/route-enp0s9
10.3.1.0/24 via 10.3.100.1 dev enp0s9
```
Exemple ping de 10.3.1.11 (net1) vers 10.3.2.11 (net2) (et inversement):

```bash
[mirak@node1 ~]$ hostname
node1.net1.tp3
[mirak@node1 ~]$ ping 10.3.2.11
PING 10.3.2.11 (10.3.2.11) 56(84) bytes of data.
64 bytes from 10.3.2.11: icmp_seq=1 ttl=62 time=6.04 ms
^C
--- 10.3.2.11 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 6.040/6.040/6.040/0.000 ms
```
L'inverse :

```bash
[mirak@node1 ~]$ hostname
node1.net2.tp3
[mirak@node1 ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=62 time=6.58 ms
^C
--- 10.3.1.11 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 6.580/6.580/6.580/0.000 ms
```

🌞 **Mettre en place les routes par défaut**

- faire en sorte que toutes les machines de votre topologie aient un accès internet, il faut donc :
  - sur les machines du réseau 1, ajouter `router.net1.tp3` comme passerelle par défaut : 
```bash 
  [mirak@node1 ~]$ hostname
node1.net1.tp3
[mirak@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.1.11
NETMASK=255.255.255.0

GATEWAY=10.3.1.254
DNS1=1.1.1.1
```
```bash
[mirak@node2 ~]$ hostname
node2.net1.tp3
[mirak@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.1.12
NETMASK=255.255.255.0

GATEWAY=10.3.1.254
DNS1=1.1.1.1
```
  - sur les machines du réseau 2, ajouter `router.net2.tp3` comme passerelle par défaut :
```bash
[mirak@node1 ~]$ hostname
node1.net2.tp3
[mirak@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.2.11
NETMASK=255.255.255.0

GATEWAY=10.3.2.254
DNS1=1.1.1.1
```
```bash
[mirak@node2 ~]$ hostname
node2.net2.tp3
[mirak@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
NAME=enp0s3
DEVICE=enp0s3

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.2.12
NETMASK=255.255.255.0

GATEWAY=10.3.2.254
DNS1=1.1.1.1
```

  
  - sur `router.net2.tp3`, ajouter `router.net1.tp3` comme passerelle par défaut :
```bash
[mirak@router ~]$ hostname
router.net2.tp3
[mirak@router ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s9
NAME=enp0s9
DEVICE=enp0s9

BOOTPROTO=static
ONBOOT=yes

IPADDR=10.3.100.2
NETMASK=255.255.255.252

GATEWAY=10.3.100.1
DNS1=1.1.1.1
```
- prouvez avec un `ping` depuis `node1.net1.tp3` que vous avez bien un accès internet :

```bash
[mirak@node1 ~]$ hostname
node1.net1.tp3
[mirak@node1 ~]$ ping google.com
PING google.com (74.125.24.101) 56(84) bytes of data.
64 bytes from sf-in-f101.1e100.net (74.125.24.101): icmp_seq=2 ttl=127 time=635 ms
^C64 bytes from 74.125.24.101: icmp_seq=3 ttl=127 time=617 ms

--- google.com ping statistics ---
3 packets transmitted, 2 received, 33.3333% packet loss, time 7403ms
rtt min/avg/max/mdev = 616.977/625.937/634.898/8.960 ms
```

- prouvez avec un `traceroute` depuis `node2.net2.tp3` que vous avez bien un accès internet, et que vos paquets transitent bien par `router2.tp3` puis par `router1.tp3` avant de sortir vers internet :
```bash
[mirak@node1 ~]$ hostname
node1.net2.tp3
[mirak@node1 ~]$ ping google.com
PING google.com (172.217.194.113) 56(84) bytes of data.
64 bytes from si-in-f113.1e100.net (172.217.194.113): icmp_seq=1 ttl=126 time=607 ms
^C64 bytes from 172.217.194.113: icmp_seq=2 ttl=126 time=752 ms

--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1082ms
rtt min/avg/max/mdev = 607.496/679.695/751.895/72.199 ms
```
```bash
[mirak@node2 ~]$ tracepath -b google.com
 1?: [LOCALHOST]                      pmtu 1500
 1:  _gateway (10.3.2.254)                                 4.618ms
 1:  _gateway (10.3.2.254)                                 2.325ms
 2:  10.3.100.1 (10.3.100.1)                               6.375ms
 3:  192.168.11.2 (192.168.11.2)                           3.893ms
 4:  no reply
```

# I. DHCP



🌞 **Setup de la machine `dhcp.net1.tp3`**

Fichier conf dhcpd :
```bash
[mirak@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
# create new
# specify domain name
option domain-name     "srv.world";
# specify DNS server's hostname or IP address
option domain-name-servers     1.1.1.1;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.3.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.50 10.3.1.99;
    # specify broadcast address
    option broadcast-address 10.3.1.255;
    # specify gateway
    option routers 10.3.1.254;
}
```

🌞 **Preuve !**

-ip a `node1.net1.tp3` ;
```bash
[mirak@node1 ~]$ hostname
node1.net1.tp3
[mirak@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:12:16:6f brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.50/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 433sec preferred_lft 433sec
    inet6 fe80::a00:27ff:fe12:166f/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d5:be:f7 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.107/24 brd 192.168.56.255 scope global dynamic noprefixroute enp0s8
       valid_lft 372sec preferred_lft 372sec
    inet6 fe80::b890:9b43:d514:89e0/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

```
c'est bien une ip de la plage dhcp '10.3.1.50/24'

- montrez que votre table de routage a été mise à jour :
```bash
[mirak@node1 ~]$ ip route show
default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.50 metric 102
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.50 metric 102
192.168.56.0/24 dev enp0s8 proto kernel scope link src 192.168.56.107 metric 101
```
- montrez l'adresse du serveur DNS que vous utilisez :
```bash
[mirak@node1 ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search srv.world net1.tp3
nameserver 1.1.1.1
```
- prouvez que vous avez un accès internet normal avec un `ping`
```bash
[mirak@node1 ~]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=127 time=44.8 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=127 time=27.1 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=3 ttl=127 time=31.2 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2006ms
rtt min/avg/max/mdev = 27.060/34.353/44.841/7.602 ms
```


# II. Serveur Web

- [Mise en place de la topologie et routage](#mise-en-place-de-la-topologie-et-routage)
  - [I. Setup GNS3](#i-setup-gns3)
  - [II. Routes routes routes](#ii-routes-routes-routes)
- [I. DHCP](#i-dhcp)
- [II. Serveur Web](#ii-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Page HTML et racine web](#2-page-html-et-racine-web)
  - [3. Config de NGINX](#3-config-de-nginx)
  - [4. Firewall](#4-firewall)
  - [5. Test](#5-test)
- [III. Serveur DNS](#iii-serveur-dns)
  - [1. Firewall](#1-firewall)
  - [2. DHCP my old friend](#2-dhcp-my-old-friend)




## 1. Installation



🌞 **Installation du serveur web NGINX**

- installez le paquet `nginx` :
```bash
sudo dnf -y install nginx 
```

## 2. Page HTML et racine web

🌞 **Création d'une bête page HTML**

```bash
mkdir /var/www/efrei_site_nul

sudo vim /var/www/efrei_site_nul/index.html
cat /var/www/efrei_site_nul/index.html
Hellooooo
```
```bash
sudo chown -R nginx /var/www/efrei_site_nul/index.html
```


## 3. Config de NGINX

🌞 **Création d'un fichier de configuration NGINX**

- `/etc/nginx/conf.d/web.net2.tp3.conf`  :

```nginx
  server {
      # on indique le nom d'hôte du serveur
      server_name   web.net2.tp3;

      # on précise sur quelle IP et quel port on veut que le site soit dispo
      listen        10.3.2.101:80;

      location      / {
          # on indique l'endroit où se trouve notre racine web
          root      /var/www/efrei_site_nul;

          # et on indique le nom de la page d'accueil, pour pas que le client ait besoin de le préciser explicitement
          index index.html;
      }
  }
```

## 4. Firewall

🌞 **Ouvrir le port nécessaire dans le firewall**

```bash
[mirak@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[mirak@web ~]$ sudo firewall-cmd --reload
success
[mirak@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: yes
```

## 5. Test

🌞 **Démarrez le service NGINX !**

- s'il y a des soucis, lisez bien les lignes d'erreur, et n'hésitez pas à m'appeler

```bash
# Démarrez le service tout de suite
$ sudo systemctl start nginx

# Faire en sorte que le service démarre tout seul quand la VM s'allume
$ sudo systemctl enable nginx

# Obtenir des infos sur le service
$ sudo systemctl status nginx

# Obtenir des logs en cas de probème
$ sudo journalctl -xe -u nginx
```

🌞 **Test local**
```bash
[mirak@web ~]$ curl http://10.3.2.101
Hellooooo
```

🌞 **Accéder au site web depuis un client**

```bash
[mirak@node1 ~]$ hostname
node1.net1.tp3
[mirak@node1 ~]$ curl http://10.3.2.101
Hellooooo
```

🌞 **Avec un nom ?**

```bash 
[mirak@node1 ~]$ curl http://web.net2.tp3
Hellooooo
[mirak@node1 ~]$ sudo cat /etc/hosts
10.3.2.101  web.net2.tp3
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
[mirak@node1 ~]$
```

# III. Serveur DNS



## 1. Firewall

🌞 **Ouvrir le port nécessaire dans le firewall**

```bash
[mirak@dns ~]$ sudo firewall-cmd --add-port=53/udp --permanent
success
[mirak@dns ~]$ sudo firewall-cmd --reload
success
[mirak@dns ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 53/udp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```



🌞 **Depuis l'une des machines clientes du réseau 1** (par exemple `node1.net1.tp3`)

```bash
[mirak@node1 ~]$ dig @10.3.2.102 web.net2.tp3

; <<>> DiG 9.16.23-RH <<>> @10.3.2.102 web.net2.tp3
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 57027
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: a9ccb6a440ccee1101000000653fb2893e27addc68799fe6 (good)
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; ANSWER SECTION:
web.net2.tp3.           86400   IN      A       10.3.2.101

;; Query time: 12 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Mon Oct 30 14:41:29 CET 2023
;; MSG SIZE  rcvd: 85

[mirak@node1 ~]$ curl web.net2.tp3
Hellooooo
```

## 2. DHCP my old friend

🌞 **Editez la configuration du serveur DHCP sur `dhcp.net1.tp3`**

```bash
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
# create new
# specify domain name
option domain-name     "srv.world";
# specify DNS server's hostname or IP address
option domain-name-servers     10.3.2.102;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.3.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.1.50 10.3.1.99;
    # specify broadcast address
    option broadcast-address 10.3.1.255;
    # specify gateway
    option routers 10.3.1.254;
}
~                                                                                                                       ~                                                                                                                       ~   
```
```bash
[mirak@node1 ~]$ dig web.net2.tp3

; <<>> DiG 9.16.23-RH <<>> web.net2.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 50825
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: c726ccc29020250001000000653fb8757c7e5f35e56bc98f (good)
;; QUESTION SECTION:
;web.net2.tp3.                  IN      A

;; ANSWER SECTION:
web.net2.tp3.           86400   IN      A       10.3.2.101

;; Query time: 8 msec
;; SERVER: 10.3.2.102#53(10.3.2.102)
;; WHEN: Mon Oct 30 15:06:45 CET 2023
;; MSG SIZE  rcvd: 85

```